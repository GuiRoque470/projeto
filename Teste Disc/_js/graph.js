var discChart = document.getElementById("graph");

var ctx1 = discChart.getContext("2d");

Chart.defaults.global.defaultFontFamily = "Sintony";
Chart.defaults.global.defaultFontColor = "grey";
Chart.defaults.global.defaultFontSize = 12;



var profileChart = new Chart(ctx1, {
    type: "line",
    plugins: [{
    beforeDraw: chart => {
      var ctx = chart.chart.ctx;
      var xAxis = chart.scales['x-axis-0'];
      var yAxis = chart.scales['first-y-axis'];
      var yAxis2 = chart.scales['third-y-axis'];       
      ctx.save();            
      ctx.fillStyle  = 'lightgray';
      ctx.beginPath();    
      var yTop = yAxis.getPixelForValue(16.5);
      var yBottom = yAxis.getPixelForValue(12.5);
      ctx.fillRect(xAxis.left, yTop, xAxis.right - xAxis.left, yBottom - yTop);

      
      ctx.stroke();            
      ctx.restore();
    },
    
  }],
    data: {
        labels: ["", "D", "I", "S", "C", ""],
        datasets:[{
            data: [, 21, 15.5, 12, 5, ],
            fill: false,
            borderWidth: 1,
            pointBackgroundColor: "black",
            backgroundColor: "black",
            borderColor: "black",
            lineTension: 0, 
            yAxisID: 'first-y-axis'
        },
        {
            yAxisID: 'third-y-axis'
        }],
       
    },
    options: {
        responsive: false,
        
        title: {
            display: true,
            text: 'Gráfico do Perfil DISC',
            fontSize: 20,
        },
        scales: {
            yAxes: [{
                id: 'first-y-axis',
                type: 'linear',
                gridLines: {
                    color: "grey",
                    drawOnChartArea: false
                },
                scaleLabel: {
                    display: true,
                    padding: '15px',
                    labelString: 'Intensity'
                  },
                ticks: {
                    max: 28,
                    min: 1,
                    stepSize: 1
                }
            },
            {
                id: 'second-y-axis',
                type: 'linear',
                position: 'left',
                display: false,
                gridLines: {
                    color: "grey",
                    drawOnChartArea: false
                },
                ticks: {
                    display: false,
                    min: 1,
                    max: 8,
                    stepSize: 1
                }
            },
            {
                id: 'third-y-axis',
                position: 'right',
                type: 'linear',
                gridLines: {
                    color: "grey",
                    drawOnChartArea: false
                },
                scaleLabel: {
                    display: true,
                    padding: '10px',
                    labelString: 'Segment'
                  },
                ticks: {
                    max: 7.5,
                    min: 0.5,
                    stepSize: 1
                },
                afterTickToLabelConversion: function(scaleInstance) {
                    scaleInstance.ticks[0] = null;
                    scaleInstance.ticks[scaleInstance.ticks.length - 1] = null;
                    scaleInstance.ticksAsNumbers[0] = null;
                    scaleInstance.ticksAsNumbers[scaleInstance.ticksAsNumbers.length - 1] = null;
                  },
            }] 
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false,
            callbacks: {
               label: function(tooltipItem) {
                      return tooltipItem.yLabel;
               }
            }
        }
    }
});

function addData(chart, data) {
    
    chart.data.datasets[0].data[1] = data[0];
    chart.data.datasets[0].data[2] = data[1];
    chart.data.datasets[0].data[3] = data[2];
    chart.data.datasets[0].data[4] = data[3];
    chart.update();
}
dataChange = [];
dataMost = [];
dataLeast = [];

function ShowGraph(num){
    if(num == 1){
        for(var i = 0; i < dataChange.length; i++){
            console.log(dataChange[i]);
        }
        
        addData(profileChart, dataChange);
    }
    else if(num == 2){
        for(var i = 0; i < dataMost.length; i++){
            console.log(dataMost[i]);
        }
        
        addData(profileChart, dataMost);
    }
    else if(num == 3){
        for(var i = 0; i < dataLeast.length; i++){
            console.log(dataLeast[i]);
        }
        
        addData(profileChart, dataLeast);
    }
}

function DadosGraficoChange(d, i, s, c){
    var numD = 0;
    if(d == -27){
        numD = 1;
    }
    else if(d >= -26 && d <= - 17){
        numD = 2;
    }
    else if(d == -16){
        numD = 3;
    }
     else if(d == -15){
        numD = 4;
    }
    else if(d == -14){
        numD = 5.5;
    }
    else if(d == -13){
        numD = 7;
    }
    else if(d == -12){
        numD = 8;
    }
    else if(d == -11){
        numD = 9;
    }
    else if(d == -10){
        numD = 10;
    }
    else if(d == -9){
        numD = 11;
    }
    else if(d == -8){
        numD = 12;
    }
    else if(d == -7){
        numD = 14;
    }
    else if(d == -6){
        numD = 15;
    }
    else if(d == -5){
        numD = 16;
    }
    else if(d == -4){
        numD = 17.5;
    }
    else if(d == -3){
        numD = 18.5;
    }
    else if(d == -2){
        numD = 19;
    }
    else if(d == -1){
        numD = 20;
    }
    else if(d == 0){
        numD = 21;
    }
    else if(d == 1){
        numD = 22;
    }
    else if(d == 2){
        numD = 22.5;
    }
    else if(d == 3){
        numD = 23;
    }
    else if(d == 4){
        numD = 23.5;
    }
    else if(d == 5){
        numD = 24;
    }
    else if(d == 6){
        numD = 25;
    }
    else if(d > 6 && d < 9){
        numD = 25.5;
    }
    else if(d == 9){
        numD = 26;
    }
    else if(d > 9 && d < 27){
        numD = 27;
    }
    else if(d == 27){
        numD = 28;
    }

    var numI = 0;

    if(i == -26){
        numI = 1;
    }
    else if(i >= -25 && i <= - 9){
        numI = 2;
    }
    else if(i == -8){
        numI = 3;
    }
     else if(i == -6){
        numI = 4;
    }
    else if(i == -5){
        numI = 5.5;
    }
    else if(i == -4){
        numI = 6.5;
    }
    else if(i == -3){
        numI = 8;
    }
    else if(i == -2){
        numI = 9.5;
    }
    else if(i == -1){
        numI = 11;
    }
    else if(i == 0){
        numI = 12;
    }
    else if(i == 1){
        numI = 14.5;
    }
    else if(i == 2){
        numI = 15.5;
    }
    else if(i == 3){
        numI = 17.5;
    }
    else if(i == 4){
        numI = 19;
    }
    else if(i == 5){
        numI = 20;
    }
    else if(i == 6){
        numI = 21.5;
    }
    else if(i == 7){
        numI = 23.5;
    }
    else if(i == 8){
        numI = 25;
    }
    else if(i == 9){
        numI = 26;
    }
    else if(i > 9 && i < 28){
        numI = 27;
    }
    else if(i == 28){
        numI = 28;
    }

    var numS = 0;
    if(s == -27){
        numS = 1;
    }
    else if(s >= -26 && s <= - 8){
        numS = 2;
    }
    else if(s == -7){
        numS = 3;
    }
     else if(s == -6){
        numS = 3.5;
    }
    else if(s == -5){
        numS = 4;
    }
    else if(s == -4){
        numS = 5;
    }
    else if(s == -3){
        numS = 7;
    }
    else if(s == -2){
        numS = 7.5;
    }
    else if(s == -1){
        numS = 8;
    }
    else if(s == 0){
        numS = 9.5;
    }
    else if(s == 1){
        numS = 11;
    }
    else if(s == 2){
        numS = 12;
    }
    else if(s == 3){
        numS = 13.5;
    }
    else if(s == 4){
        numS = 14;
    }
    else if(s == 5){
        numS = 15;
    }
    else if(s == 6){
        numS = 17;
    }
    else if(s == 7){
        numS = 18.5;
    }
    else if(s == 8){
        numS = 20;
    }
    else if(s == 9){
        numS = 21;
    }
    else if(s == 10){
        numS = 22.5;
    }
    else if(s == 11){
        numS = 24;
    }
    else if(s == 12){
        numS = 25;
    }
    else if(s == 13){
        numS = 26;
    }
    else if(s == 14){
        numS = 26.5;
    }
    else if(s > 14 && s < 25){
        numS = 27;
    }
    else if(s == 26){
        numS = 28;
    }

    var numC = 0;
    if(c == -26){
        numC = 1;
    }
    else if(c >= -25 && c <= - 12){
        numC = 2;
    }
    else if(c == -11){
        numC = 2.5;
    }
    else if(c == -10){
        numC = 3;
    }
     else if(c == -9){
        numC = 4;
    }
    else if(c == -8){
        numC = 5;
    }
    else if(c == -7){
        numC = 6.5;
    }
    else if(c == -6){
        numC = 8;
    }
    else if(c == -5){
        numC = 9.5;
    }
    else if(c == -4){
        numC = 11;
    }
    else if(c == -3){
        numC = 12;
    }
    else if(c == -2){
        numC = 15;
    }
    else if(c == -1){
        numC = 16;
    }
    else if(c == 0){
        numC = 18;
    }
    else if(c == 1){
        numC = 18.5;
    }
    else if(c == 2){
        numC = 20;
    }
    else if(c == 3){
        numC = 21;
    }
    else if(c == 4){
        numC = 22.5;
    }
    else if(c == 5){
        numC = 23.5;
    }
    else if(c == 6){
        numC = 25;
    }
    else if(c >= 7 && c <= 17){
        numC = 26;
    }
    else if(c == 18){
        numC = 27;
    }
    else if(c > 18 && c < 24){
        numC = 27.5;
    }
    else if(c == 24){
        numC = 28;
    }
    
    dataChange.push(numD, numI, numS, numC);
}

function DadosGraficoMost(d, i, s, c){
    var numD = 0;
    if(d == 0){
        numD = 3;
    } 
    else if(d == 1){
        numD = 5.5;
    }
    else if(d == 2){
        numD = 9;
    } 
    else if(d == 3){
        numD = 12;
    }
    else if(d == 4){
        numD = 14;
    }
    else if(d == 5){
        numD = 16;
    }  
    else if(d == 6){
        numD = 19;
    }
    else if(d == 7){
        numD = 20;
    }   
    else if(d == 8){
        numD = 22;
    }
    else if(d == 9){
        numD = 23;
    }
    else if(d == 10){
        numD = 24;
    }
    else if(d == 11){
        numD = 25;
    }
    else if(d == 12){
        numD = 26;
    }
    else if(d >= 13 && d <= 26){
        numD = 27;
    }
    else if(d == 27){
        numD = 28;
    }

    var numI = 0;
    if(i == 0){
        numI = 1;}
    else if(i == 1){
        numI = 2;}
    else if(i == 2){
        numI = 3.5;}
    else if(i == 3){
        numI = 6;}
    else if(i == 4){
        numI = 9;}
    else if(i == 5){
        numI = 12;}
    else if(i == 6){
        numI = 14;}
    else if(i == 7){
        numI = 16;}
    else if(i == 8){
        numI = 19.5;}
    else if(i == 9){
        numI = 22;}
    else if(i == 10){
        numI = 23;}
    else if(i == 11){
        numI = 25;}
    else if(i == 12){
        numI = 26;}
    else if(i >= 13 && i <= 27){
        numI = 27;}
    else if(i == 28){
        numI = 28;}

    var numS = 0;
    if(s == 0){
        numS = 1;}
    else if(s == 1){
        numS = 2;}
    else if(s == 2){
        numS = 3;}
    else if(s == 3){
        numS = 4;}
    else if(s == 4){
        numS = 7;}
    else if(s == 5){
        numS = 10.5;}
    else if(s == 6){
        numS = 11;}
    else if(s == 7){
        numS = 13;}
    else if(s == 8){
        numS = 15;}
    else if(s == 9){
        numS = 17;}
    else if(s == 10){
        numS = 19;}
    else if(s == 11){
        numS = 21;}
    else if(s == 12){
        numS = 23;}
    else if(s == 13){
        numS = 25;}
    else if(s == 14){
        numS = 26;}
    else if(s >= 15 && s <= 25){
        numS = 27;}
    else if(s == 26){
        numS = 28;}

    var numC = 0;
    if(c == 0){
        numC = 1;}
    else if(c == 1){
        numC = 2;}
    else if(c == 2){
        numC = 4;}
    else if(c == 3){
        numC = 7.5;}
    else if(c == 4){
        numC = 10.5;}
    else if(c == 5){
        numC = 13;}
    else if(c == 6){
        numC = 16;}
    else if(c == 7){
        numC = 19;}
    else if(c == 8){
        numC = 21;}
    else if(c == 9){
        numC = 23;}
    else if(c == 10){
        numC = 24;}
    else if(c == 11){
        numC = 26;}
    else if(c == 12){
        numC = 27;}
    else if(c >= 13 && c <= 23){
        numC = 27.5;}
    else if(c == 24){
        numC = 28;}
    
    dataMost.push(numD, numI, numS, numC);
    
}

function DadosGraficoLeast(d, i, s, c){
    var numD = 0;
    if(d == 27){
        numD = 1;}
    else if(d <= 26 && d >= 18){
        numD = 2;}
    else if(d == 17){
        numD = 3;}
    else if(d == 16){
        numD = 4;}
    else if(d == 15){
        numD = 5;}
    else if(d == 14){
        numD = 7;}
    else if(d == 13){
        numD = 9.5;}
    else if(d == 12){
        numD = 11;}
    else if(d == 11){
        numD = 12;}
    else if(d == 10){
        numD = 14;}
    else if(d == 9){
        numD = 18;}
    else if(d == 8){
        numD = 19.5;}
    else if(d == 7){
        numD = 21;}
    else if(d == 6){
        numD = 22;}
    else if(d == 5){
        numD = 24;}
    else if(d == 4){
        numD = 25;}
    else if(d == 3){
        numD = 26;}
    else if(d == 2){
        numD = 26.5;}
    else if(d == 1){
        numD = 27;}
    else if(d == 0){
        numD = 28;}
    
    var numI = 0;
    if(i == 26){
        numI = 1;}
    else if(i <= 25 && i >= 11){
        numI = 2;}
    else if(i == 10){
        numI = 3;}
    else if(i == 9){
        numI = 4;}
    else if(i == 8){
        numI = 7;}
    else if(i == 7){
        numI = 10;}
    else if(i == 6){
        numI = 12;}
    else if(i == 5){
        numI = 16;}
    else if(i == 4){
        numI = 20;}
    else if(i == 3){
        numI = 23;}
    else if(i == 2){
        numI = 25;}
    else if(i == 1){
        numI = 26.5;}
    else if(i == 0){
        numI = 28;}
    
    var numS = 0;
    if(s == 27){
        numS = 1;}
    else if(s <= 26 && s >= 11){
        numS = 2;}
    else if(s == 10){
        numS = 3.5;}
    else if(s == 9){
        numS = 4;}
    else if(s == 8){
        numS = 5.5;}
    else if(s == 7){
        numS = 7;}
    else if(s == 6){
        numS = 10.5;}
    else if(s == 5){
        numS = 12;}
    else if(s == 4){
        numS = 15;}
    else if(s == 3){
        numS = 19;}
    else if(s == 2){
        numS = 22;}
    else if(s == 1){
        numS = 26;}
    else if(s == 0){
        numS = 28;}

    var numC = 0;
    if(c == 26){
        numC = 1;}
    else if(c <= 25 && c >= 12){
        numC = 2;}
    else if(c == 3){
        numC = 26;}
    else if(c == 12){
        numC = 4;}
    else if(c == 11){
        numC = 6;}
    else if(c == 10){
        numC = 8;}
    else if(c == 9){
        numC = 11;}
    else if(c == 8){
        numC = 14;}
    else if(c == 7){
        numC = 16;}
    else if(c == 6){
        numC = 20;}
    else if(c == 5){
        numC = 22;}
    else if(c == 4){
        numC = 24;}
    else if(c == 3){
        numC = 25;}
    else if(c == 2){
        numC = 26;}
    else if(c == 1){
        numC = 27;}
    else if(c == 0){
        numC = 28;}
    
    dataLeast.push(numD, numI, numS, numC);
}
