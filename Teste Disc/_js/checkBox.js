var paginaAtual = [];
$(document).ready(function(){
    
    var num = numeros.pop();
    var id1 = "#t" + num;
    if($(id1).css("display", "none")){
        $(id1).css("display", "block");
    }
    paginaAtual.push(num);
    $(".optionCounter").html(paginaAtual.length + " / 28");
    if ($(window).width() < 990){
        $(".container-box-body").css("padding", "0 10px 30px");
        $(".tableHeader").css("font-size", "10px");
        $(".userResultText").css("width", "130%");
        $(".tableHeader").css("width", "80px");
        $(".barBackground").css("width", "280px");
        $(".barForeground").css("width", paginaAtual.length*10 +"px");
        $(".resultPage").css("display", "block");
        $("#graph").css("width", "285px");
        $("#graph").css("height", "450px");
        $("#error_message").css("width", "85%");
    }
    else{
        $("error_message").css("width", "350px");
        $(".resultPage").css("display", "flex");
        $("#graph").css("width", "380px");
        $("#graph").css("height", "550px");
        $(".container-box-body").css("padding", "30px");
        $(".tableHeader").css("font-size", "12px");
        $(".userResultText").css("width", "100%");
        $(".tableHeader").css("width", "100px");
        $(".barBackground").css("width", "392px");
        $(".barForeground").css("width", paginaAtual.length*14 +"px");
    }

});

window.onresize = function(event) {
    if ($(window).width() < 992){
    $("#error_message").css("width", "85%");
    $(".barBackground").css("width", "280px");
    $(".barForeground").css("width", paginaAtual.length*10 +"px");
    }
    else{
        $("error_message").css("width", "350px");
        $(".barBackground").css("width", "392px");
        $(".barForeground").css("width", paginaAtual.length*14 +"px");
    }

};

function checkOption(valor){
    if (valor == 11){
        ClearPositiveChecked();
        if($(".secondOption1").hasClass("CheckedNegative") == true){
            ClearNegativeChecked();
            $(".firstOption1").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption1 CheckedPositiveInt"/>').appendTo('.firstOption1');}
        else{
            $(".firstOption1").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption1 CheckedPositiveInt"/>').appendTo('.firstOption1');
        }
    }
    else if (valor == 12){
        ClearNegativeChecked();
        if($(".firstOption1").hasClass("CheckedPositive") == true){
            ClearPositiveChecked();
            $(".secondOption1").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption1 CheckedNegativeInt"/>').appendTo('.secondOption1');}
        else{
            $(".secondOption1").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption1 CheckedNegativeInt"/>').appendTo('.secondOption1');
        }  
    }
    if (valor == 21){
        ClearPositiveChecked();
        if($(".secondOption2").hasClass("CheckedNegative") == true){
            ClearNegativeChecked();
            $(".firstOption2").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption2 CheckedPositiveInt"/>').appendTo('.firstOption2');}
        else{
            $(".firstOption2").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption2 CheckedPositiveInt"/>').appendTo('.firstOption2');
        }
    }
    else if (valor == 22){
        ClearNegativeChecked();
        if($(".firstOption2").hasClass("CheckedPositive") == true){
            ClearPositiveChecked();
            $(".secondOption2").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption2 CheckedNegativeInt"/>').appendTo('.secondOption2');}
        else{
            $(".secondOption2").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption2 CheckedNegativeInt"/>').appendTo('.secondOption2');
        }  
    }
    if (valor == 31){
        ClearPositiveChecked();
        if($(".secondOption3").hasClass("CheckedNegative") == true){
            ClearNegativeChecked();
            $(".firstOption3").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption3 CheckedPositiveInt"/>').appendTo('.firstOption3');}
        else{
            $(".firstOption3").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption3 CheckedPositiveInt"/>').appendTo('.firstOption3');
        }
    }
    else if (valor == 32){
        ClearNegativeChecked();
        if($(".firstOption3").hasClass("CheckedPositive") == true){
            ClearPositiveChecked();
            $(".secondOption3").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption3 CheckedNegativeInt"/>').appendTo('.secondOption3');}
        else{
            $(".secondOption3").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption3 CheckedNegativeInt"/>').appendTo('.secondOption3');
        }  
    }
    if (valor == 41){
        ClearPositiveChecked();
        if($(".secondOption4").hasClass("CheckedNegative") == true){
            ClearNegativeChecked();
            $(".firstOption4").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption4 CheckedPositiveInt"/>').appendTo('.firstOption4');}
        else{
            $(".firstOption4").removeClass("NotChecked").addClass("CheckedPositive");
            $('<div class="firstOption4 CheckedPositiveInt"/>').appendTo('.firstOption4');
        }
    }
    else if (valor == 42){
        ClearNegativeChecked();
        if($(".firstOption4").hasClass("CheckedPositive") == true){
            ClearPositiveChecked();
            $(".secondOption4").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption4 CheckedNegativeInt"/>').appendTo('.secondOption4');}
        else{
            $(".secondOption4").removeClass("NotChecked").addClass("CheckedNegative");
            $('<div class="secondOption4 CheckedNegativeInt"/>').appendTo('.secondOption4');
        }  
    }
}

var inputList = [];

function RegisterInput(page_id){
    var id = "#t" + page_id;
    var val1 = "";
    var val2 = "";
    val1 = $(id).find(".CheckedPositive").attr("data-value");
    val2 = $(id).find(".CheckedNegative").attr("data-value");
    
    if (val1 == null || val2 == null){
        return false;
    }    
    else{
        inputList.push(val1);
        inputList.push(val2);
        return true;
    }
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

var numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 26, 27, 28];
shuffle(numeros);


function MudaPagina(){
    var num2 = numeros.pop();     //Pega um número da lista embaralhada para escolher a próxima página

    var index = paginaAtual.length - 1;
    var num1 = paginaAtual[index];

    if(paginaAtual.length < 28){   //Verifica se acabaram as páginas ou não.
        var empty = false;
    }
    else{
        var empty = true;
    }
    confirma(num1, num2, empty);
}


function confirma(num1, num2, empty){
    var page_id = num1;
    var result = RegisterInput(page_id);  //Checa se o usuário não deixou de marcar todas as opções obrigatórias
    
    if (result != false){
        ClearPositiveChecked();
        ClearNegativeChecked();
        $(".error_message").css("display", "none");
        var id2 = "#t" + num2;
        var id1 = "#t" + num1;

        if(empty != true){
            
            paginaAtual.push(num2);
            $(id1).css("display","none");
            $(".optionCounter").html(paginaAtual.length + " / 28");
            if ($(window).width() < 992){
                $(".barBackground").css("width", "280px");
                $(".barForeground").css("width", paginaAtual.length*10 +"px");
            }
            else{
                $(".barBackground").css("width", "392px");
                $(".barForeground").css("width", paginaAtual.length*14 +"px");
            }
            $(id2).css("display","block");
        }
        else{
            $(id1).css("display","none");
            $(".container-box-buttonQuestion").css("display", "none");
            $(".barSpace").css("display", "none");
            $(".container-box-body").css("padding", "0 30px 30px");

            GenerateResult(inputList);

            $("#footerContainer2").css("display", "block");
            $("#footerContainer1").css("display", "none");
            $("#footerContainer2").css("position", "relative");

            $(".container-box-textQuestion").css("display", "none");
            $("#resultaPagina").css("display","block");
        }
    }
    else{
        $(".error_message").css("display", "flex");
        $(".error_message").empty().html("");
        $(".error_message").append("É preciso selecionar ambas as opções antes de prosseguir!");
        numeros.push(num2);}
}

function ChangePageTo(num){
    if(num == 1){
        $(".containerResultsBarForeground").css("left", "0");
        $(".optionTitle").html("<div><b>MUDANÇA</b></div>");
        $(".least").css("display", "none");
        $(".most").css("display", "none");
        $(".change").css("display", "block");
        ShowGraph(1);
    }
    else if(num == 2){
        $(".containerResultsBarForeground").css("left", "34%");
        $(".optionTitle").html("<div><b>MAIS</b></div>");
        $(".change").css("display", "none");
        $(".least").css("display", "none");
        $(".most").css("display", "block");
        ShowGraph(2);

    }
    else if(num == 3){
        $(".containerResultsBarForeground").css("left", "68%");
        $(".optionTitle").html("<div><b>MENOS</b></div>");
        $(".change").css("display", "none");
        $(".most").css("display", "none");
        $(".least").css("display", "block");
        ShowGraph(3);
    }
}

function ClearPositiveChecked(){
    $(".CheckedPositive").removeClass("CheckedPositive").addClass("NotChecked");
    $(".CheckedPositiveInt").remove();
}

function ClearNegativeChecked(){
    $(".CheckedNegative").removeClass("CheckedNegative").addClass("NotChecked");
    $(".CheckedNegativeInt").remove();
}