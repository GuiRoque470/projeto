function GenerateResult(lista){
    var dCountMost = 0;   var dCountLeast = 0;
    var iCountMost = 0;   var iCountLeast = 0;
    var sCountMost = 0;   var sCountLeast = 0;
    var cCountMost = 0;   var cCountLeast = 0;

    //<--Contando quantos D's, I's, S's e C's aparecerem como Mais-->
    for(var i = 0; i<lista.length; i+= 2){
        if(lista[i] == "D"){
            dCountMost += 1;
            console.log("Achei " + dCountMost + " D's em Mais até agora!");
        }
        if(lista[i] == "I"){
            iCountMost += 1;
            console.log("Achei " + iCountMost + " I's em Mais até agora!");
        }
        if(lista[i] == "S"){
            sCountMost += 1;
            console.log("Achei " + sCountMost + " S's em Mais até agora!");
        }
        if(lista[i] == "C"){
            cCountMost += 1;
            console.log("Achei " + cCountMost + " C's em Mais até agora!");
        }
    }
    //<--Contando quantos D's, I's, S's e C's aparecerem como Menos-->
    for(var i = 1; i<lista.length; i+= 2){
        if(lista[i] == "D"){
            dCountLeast += 1;
            console.log("Achei " + dCountLeast + " D's em Menos até agora!");}
        if(lista[i] == "I"){
            iCountLeast += 1;
            console.log("Achei " + iCountLeast + " I's em Menos até agora!");}
        if(lista[i] == "S"){
            sCountLeast += 1;
            console.log("Achei " + sCountLeast + " S's em Menos até agora!");}
        if(lista[i] == "C"){
            cCountLeast += 1;
            console.log("Achei " + cCountLeast + " C's em Menos até agora!");}
    }
    //<--Calculo da média entre resultados Mais e Menos-->//
    var dCount = dCountMost - dCountLeast;
    console.log(dCountMost + " - " + dCountLeast + " = " + dCount);
    var iCount = iCountMost - iCountLeast;
    console.log(iCountMost + " - " + iCountLeast + " = " + iCount);
    var sCount = sCountMost - sCountLeast;
    console.log(sCountMost + " - " + sCountLeast + " = " + sCount);
    var cCount = cCountMost - cCountLeast;
    console.log(cCountMost + " - " + cCountLeast + " = " + cCount);

    //<--Determinação do segmento change-->// 
    if(dCount >= -27 && dCount <= -15)
        var segment11 = 1;
    else if(dCount >= -14 && dCount <= -12)
        var segment11 = 2;
    else if(dCount >= -11 && dCount <= -8)
        var segment11 = 3;
    else if(dCount >= -7 && dCount <= -5)
        var segment11 = 4;
    else if(dCount >= -4 && dCount <= -1)
        var segment11 = 5;
    else if(dCount >= 0 && dCount <= 5)
        var segment11 = 6;
    else if(dCount >= 6 && dCount <= 27)
        var segment11 = 7;
    
    
    if(iCount >= -26 && iCount <= -6)
        var segment12 = 1;
    else if(iCount >= -5 && iCount <= -3)
        var segment12 = 2;
    else if(iCount >= -2 && iCount <= 0)
        var segment12 = 3;
    else if(iCount >= 1 && iCount <= 2)
        var segment12 = 4;
    else if(iCount >= 3 && iCount <= 5)
        var segment12 = 5;
    else if(iCount >= 6 && iCount <= 7)
        var segment12 = 6;
    else if(iCount >= 8 && iCount <= 28)
        var segment12 = 7;
    
    
    if(sCount >= -27 && sCount <= -5)
        var segment13 = 1;
    else if(sCount >= -4 && sCount <= -1)
        var segment13 = 2;
    else if(sCount >= 0 && sCount <= 2)
        var segment13 = 3;
    else if(sCount >= 3 && sCount <= 5)
        var segment13 = 4;
    else if(sCount >= 6 && sCount <= 8)
        var segment13 = 5;
    else if(sCount >= 9 && sCount <= 11)
        var segment13 = 6;
    else if(sCount >= 12 && sCount <= 26)
        var segment13 = 7;
    
        
    if(cCount >= -26 && cCount <= -9)
        var segment14 = 1;
    else if(cCount >= -8 && cCount <= -6)
        var segment14 = 2;
    else if(cCount >= -5 && cCount <= -3)
        var segment14 = 3;
    else if(cCount >= -2 && cCount <= -1)
        var segment14 = 4;
    else if(cCount >= 0 && cCount <= 2)
        var segment14 = 5;
    else if(cCount >= 3 && cCount <= 5)
        var segment14 = 6;
    else if(cCount >= 6 && cCount <= 25)
        var segment14 = 7;

    //<--Determinação do segmento most-->// 
    if(dCountMost == 0)
        var segment21 = 1;
    else if(dCountMost == 1)
        var segment21 = 2;
    else if(dCountMost == 2 || dCountMost == 3)
        var segment21 = 3;
    else if(dCountMost == 4 || dCountMost == 5)
        var segment21 = 4;
    else if(dCountMost == 7 || dCountMost == 6)
        var segment21 = 5;
    else if(dCountMost >= 8 && dCountMost <= 10)
        var segment21 = 6;
    else if(dCountMost >= 11 && dCountMost <= 27)
        var segment21 = 7;
    
    
    if(iCountMost >= 0 && iCountMost <= 2)
        var segment22 = 1;
    else if(iCountMost == 3)
        var segment22 = 2;
    else if(iCountMost == 4 || iCountMost == 5)
        var segment22 = 3;
    else if(iCountMost == 6 || iCountMost == 7)
        var segment22 = 4;
    else if(iCountMost == 8)
        var segment22 = 5;
    else if(iCountMost == 9 || iCountMost == 10)
        var segment22 = 6;
    else if(iCountMost >= 11 && iCountMost <= 28)
        var segment22 = 7;
    
    
    if(sCountMost >= 0 && sCountMost <= 3)
        var segment23 = 1;
    else if(sCountMost == 4)
        var segment23 = 2;
    else if(sCountMost == 5 || sCountMost == 6)
        var segment23 = 3;
    else if(sCountMost == 7 || sCountMost == 8)
        var segment23 = 4;
    else if(sCountMost == 9 || sCountMost == 10)
        var segment23 = 5;
    else if(sCountMost == 11 || sCountMost == 12)
        var segment23 = 6;
    else if(sCountMost >= 13 && sCountMost <= 26)
        var segment23 = 7;
    
        
    if(cCountMost >= 0 && cCountMost <= 2)
        var segment24 = 1;
    else if(cCountMost == 3)
        var segment24 = 2;
    else if(cCountMost == 4)
        var segment24 = 3;
    else if(cCountMost == 5 || cCountMost == 6)
        var segment24 = 4;
    else if(cCountMost == 7)
        var segment24 = 5;
    else if(cCountMost >= 8 && cCountMost <= 10)
        var segment24 = 6;
    else if(cCountMost >= 11 && cCountMost <= 24)
        var segment24 = 7;


    //<--Determinação do segmento least-->// 
    if(dCountLeast >= 0 && dCountLeast <= 4)
        var segment31 = 7;
    else if(dCountLeast >= 5 && dCountLeast <= 7)
        var segment31 = 6;
    else if(dCountLeast == 8 || dCountLeast == 9)
        var segment31 = 5;
    else if(dCountLeast == 10)
        var segment31 = 4;
    else if(dCountLeast >= 11 && dCountLeast <= 13)
        var segment31 = 3;
    else if(dCountLeast == 14 || dCountLeast == 15)
        var segment31 = 2;
    else if(dCountLeast >= 16 && dCountLeast <= 27)
        var segment31 = 1;
    
    
    if(iCountLeast >= 9 && iCountLeast <= 26)
        var segment32 = 1;
    else if(iCountLeast == 8)
        var segment32 = 2;
    else if(iCountLeast == 6 || iCountLeast == 7)
        var segment32 = 3;
    else if(iCountLeast == 5)
        var segment32 = 4;
    else if(iCountLeast == 4)
        var segment32 = 5;
    else if(iCountLeast == 3)
        var segment32 = 6;
    else if(iCountLeast >= 0 && iCountLeast <= 2)
        var segment32 = 7;
    
    
    if(sCountLeast >= 9 && sCountLeast <= 27)
        var segment33 = 1;
    else if(sCountLeast == 8 || sCountLeast == 7)
        var segment33 = 2;
    else if(sCountLeast == 6 || sCountLeast == 5)
        var segment33 = 3;
    else if(sCountLeast == 4)
        var segment33 = 4;
    else if(sCountLeast == 3)
        var segment33 = 5;
    else if(sCountLeast == 2)
        var segment33 = 6;
    else if(sCountLeast == 1 || sCountLeast == 0)
        var segment33 = 7;
    
        
    if(cCountLeast >= 12 && cCountLeast <= 26)
        var segment34 = 1;
    else if(cCountLeast == 11 || cCountLeast == 10)
        var segment34 = 2;
    else if(cCountLeast == 9)
        var segment34 = 3;
    else if(cCountLeast == 8 || cCountLeast == 7)
        var segment34 = 4;
    else if(cCountLeast == 6)
        var segment34 = 5;
    else if(cCountLeast == 4 || cCountLeast == 5)
        var segment34 = 6;
    else if(cCountLeast >= 0 && cCountLeast <= 3)
        var segment34 = 7;
    
    
    
    var segmentChange = segment11 + "-" + segment12 + "-" + segment13 + "-" + segment14;
    var segmentMost = segment21 + "-" + segment22 + "-" + segment23 + "-" + segment24;
    var segmentLeast = segment31 + "-" + segment32 + "-" + segment33 + "-" + segment34;

    console.log(segment11 + "-" + segment12 + "-" + segment13 + "-" + segment14);
    console.log(segment21 + "-" + segment22 + "-" + segment23 + "-" + segment24);
    console.log(segment31 + "-" + segment32 + "-" + segment33 + "-" + segment34);

    var perfilChange = GetProfile(segment11, segment12, segment13, segment14);
    var perfilMost = GetProfile(segment21, segment22, segment23, segment24);
    var perfilLeast = GetProfile(segment31, segment32, segment33, segment34);

    DadosGraficoChange(dCount, iCount, sCount, cCount);
    ShowGraph(1);
    
    $("#discLetter").html(perfilChange.letra);
    $("#segmento-change").html(segmentChange);
    $("#perfil-change").html(perfilChange.nome);
    $("#emocoes-change").html(perfilChange.emocoes);
    $("#objetivo-change").html(perfilChange.objetivo);
    $("#julga-change").html(perfilChange.julga_outros_por);
    $("#influencia-change").html(perfilChange.influencia_outros_por);
    $("#valor-change").html(perfilChange.valor_para_empresa);
    $("#excessos-change").html(perfilChange.excessos);
    $("#pressao-change").html(perfilChange.sob_pressao);
    $("#medos-change").html(perfilChange.medos);
    $("#melhoraria-change").html(perfilChange.melhoraria_produtividade_com_mais);
    $("#descricao-change").html(perfilChange.descricao);

    DadosGraficoMost(dCountMost, iCountMost, sCountMost, cCountMost);

    $("#segmento-most").html(segmentMost);
    $("#perfil-most").html(perfilMost.nome);
    $("#emocoes-most").html(perfilMost.emocoes);
    $("#objetivo-most").html(perfilMost.objetivo);
    $("#julga-most").html(perfilMost.julga_outros_por);
    $("#influencia-most").html(perfilMost.influencia_outros_por);
    $("#valor-most").html(perfilMost.valor_para_empresa);
    $("#excessos-most").html(perfilMost.excessos);
    $("#pressao-most").html(perfilMost.sob_pressao);
    $("#medos-most").html(perfilMost.medos);
    $("#melhoraria-most").html(perfilMost.melhoraria_produtividade_com_mais);
    $("#descricao-most").html(perfilMost.descricao); 

    DadosGraficoLeast(dCountLeast, iCountLeast, sCountLeast, cCountLeast);

    $("#segmento-least").html(segmentLeast);
    $("#perfil-least").html(perfilLeast.nome);
    $("#emocoes-least").html(perfilLeast.emocoes);
    $("#objetivo-least").html(perfilLeast.objetivo);
    $("#julga-least").html(perfilLeast.julga_outros_por);
    $("#influencia-least").html(perfilLeast.influencia_outros_por);
    $("#valor-least").html(perfilLeast.valor_para_empresa);
    $("#excessos-least").html(perfilLeast.excessos);
    $("#pressao-least").html(perfilLeast.sob_pressao);
    $("#medos-least").html(perfilLeast.medos);
    $("#melhoraria-least").html(perfilLeast.melhoraria_produtividade_com_mais);
    $("#descricao-least").html(perfilLeast.descricao);
 }