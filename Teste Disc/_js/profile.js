function GetProfile(d, i, s, c){

    function Profile(letra, nome, emocoes, objetivo, julga_outros_por, influencia_outros_por, valor_para_empresa, excessos, sob_pressao, medos, melhoraria_produtividade_com_mais, descricao )  {
        this.letra = letra;
        this.nome = nome;
        this.emocoes = emocoes;
        this.objetivo = objetivo;
        this.julga_outros_por = julga_outros_por;
        this.influencia_outros_por = influencia_outros_por;
        this.valor_para_empresa =valor_para_empresa;
        this.excessos = excessos;
        this.sob_pressao = sob_pressao;
        this.medos = medos;
        this.melhoraria_produtividade_com_mais = melhoraria_produtividade_com_mais;
        this.descricao = descricao;
    }
    
    
    if(d >= 5 && i <= 3 && s <= 4 && c <= 4){
        var perfil = new Profile("Dominância (D)",
                                "Desenvolvedor", 
                                "Se preocupa em atingir metas pessoais.",
                                "Novas oportunidades.",
                                "Habilidade de atingir os padrões de desenvolvedor.",
                                "Busca de soluções para problemas; Projeção de senso pessoal de poder.",
                                "Evita jogar a culpa nos outros; Busca novos ou inovadores métodos de resolver problemas ",
                                "Controle sobre pessoas ou situações para alcançar seus próprios resultados.",
                                "Trabalha sozinho para cumprir tarefas; É truculento se individualismo é ameaçado ou quando oportunidades desafiadoras desaparecem.",
                                "Tédio, perda de controle.",
                                "Paciência, empatia, participação e colaboração com outros.",
                                "Os Desenvolvedores tendem a ser indivíduos obstinados, buscando continuamente novos horizontes. Como pensadores autossuficientes e independentes, eles preferem encontrar suas próprias soluções. <br><br>Relativamente livres da influência restritiva do grupo, os Desenvolvedores são capazes de contornar as convenções e, muitas vezes, criar soluções inovadoras de bem-estar para o poder. Embora geralmente usem um comportamento direto e enérgico, os Desenvolvedores também podem manipular as pessoas e situações astutamente.  Quando solicitados a participar com outros em situações que limitam seu individualismo, os Desenvolvedores tendem a se tornar beligerantes. Eles são persistentes na busca pelos resultados que desejam e farão o que for necessário para superar os obstáculos ao sucesso. Além disso, eles têm grandes expectativas em relação aos outros e podem ser críticos quando seus padrões não são atendidos.<br><br> Os Desenvolvedores estão mais interessados ​​em atingir seus próprios objetivos. Oportunidades de avanço e desafio são importantes para eles. Ao focar nos resultados, eles podem não ter empatia ou parecer indiferentes ao ignorar as preocupações dos outros.");  //Checked
    }

    else if((d >= 5) && (i != d && i == 5 || i == 4) && (s <= 4) && (c <=4)){  //Mobilizer 2
        var perfil = new Profile("Dominância (D/I)",
                                "Orientado a Resultados",
                                "Verbaliza a força do ego; Mostra individualismo áspero.",
                                "Dominância e independência.",
                                "Habilidade de concluir tarefas rapidamente.",
                                "Carater forte; Persistência.",
                                "Persistência e obstinação.",
                                "Impaciência, competições ganha ou perde.",
                                "Se tornam críticos e apontadores de falhas; Resistência a participar com o time; Podem desrespeitar limites.",
                                "Que outras pessoas tirem vantagem de deles; Lentidão, especialmente em atividades que envolvem tarefas; Se tornar um \"puxa-saco\".",
                                "Verbalização do seu raciocínio; Consideração do ponto de vista e de idéias dos outros para solução de problemas.",
                                "Pessoas Orientadas a Resultados exibem autoconfiança, o que alguns podem interpretar como arrogância. Elas buscam ativamente oportunidades para testar e desenvolver suas habilidades para alcançar resultados. Pessoas Orientadas a Resultados gostam de tarefas difíceis, situações competitivas, atribuições exclusivas e posições importantes. Elas assumem responsabilidades com um ar de auto-importância e mostram auto-satisfação quando terminam. <br><br> Pessoas Orientadas a Resultados tendem a evitar fatores restritivos, como controle direto, detalhes demorados e trabalho de rotina. Por serem enérgicos e diretos, podem ter dificuldades com outras pessoas. Pessoas Orientadas a Resultados valorizam sua independência e podem ficar inquietas quando envolvidas em atividades de grupo ou trabalho de comitê. Embora as Pessoas Orientadas a Resultados geralmente prefiram trabalhar sozinhas, elas podem persuadir os outros a apoiarem seus esforços, especialmente ao concluir as atividades rotineiras. <br><br>As Pessoas Orientadas para a Resultados pensam rapidamente e são impacientes, considerando uma falha de outras pessoas, caso não sejam como eles. Eles avaliam os outros em sua capacidade de obter resultados. Pessoas Orientadas a Resultados são determinadas e persistentes, mesmo em face com antagonismo. Eles assumem o comando da situação quando necessário, estejam eles no comando ou não. Em sua busca intransigente por resultados, eles podem parecer rudes e misteriosos."); //Checked
    }

    else if((d >= 6) && (i >= 5 || i <= d - 1 ) && (s <= i) && (c <=4)){  //Mobilizer 1
        var perfil = new Profile("Dominância (D/I)",
                                "Inspirador",
                                "Tolera agressões; Minimiza a necessidade de afeição.",
                                "Controle de seu ambiente ou público.",
                                "Projeção de força pessoal, carater e poder social.",
                                "Charme, direção, intimidação, uso de recompensas.",
                                "Age como um motivador de pessoas; Inicia, demanda, elogia, disciplina.",
                                "\"Fins justificam os meios\".",
                                "Se tornar manipulativo, encrenqueiro ou agressivo.",
                                "Comportamento fraco; Perda de status social.",
                                "Sensitividade genuína; Disposição em fazer outros crescerem.",
                                "Pessoas com o padrão de Inspiração tentam conscientemente modificar os pensamentos e ações dos outros. Eles querem controlar seu ambiente. Eles são astutos em identificar e manipular os motivos existentes de um indivíduo a fim de direcionar o comportamento dessa pessoa para um fim predeterminado.  <br><br>Pessoas Inspiradoras são claras sobre os resultados que desejam, mas nem sempre os verbalizam imediatamente. Eles apresentam os resultados que desejam somente depois de terem preparado a outra pessoa, oferecendo amizade para aqueles que desejam aceitação, autoridade para aqueles que buscam poder e segurança para aqueles que desejam um ambiente previsível. <br><br>Pessoas Inspiradoras podem ser encantadoras em suas interações. Eles são persuasivos ao obter assistência para detalhes repetitivos e demorados. As pessoas muitas vezes experimentam uma sensação de conflito ao se sentirem atraídas por pessoas Inspiradoras e, ainda assim, curiosamente distantes, outras podem se sentir enganadas pelos poderes de manipulação das pessoas Inspiradoras. Embora às vezes inspirem medo nos outros e anulem as decisões, pessoas Inspiradoras geralmente são queridas pelos colegas porque eles usam suas consideráveis ​​habilidades verbais para persuadir os outros sempre que possível. Pessoas Inspiradoras claramente pretendem alcançar objetivos por meio de cooperação e da persuação, não da dominação."); //Checked
    }

    else if((d >= 5) && (i < d) && (s <= 4) && (c >= 5)){
        var perfil = new Profile("Dominância (D/C) ou Conformidade (C/D)",
                                "Criativo",
                                "Restringe expressões; Tolera agressões.",
                                "Dominância; Conquistas únicas.",
                                "Padrões pessoais; Idéias progressistas para se concluir tarefas.",
                                "Habilidade para acompanhar o desenvolvimento de sistemas e abordagens inovadoras.",
                                "Inicia ou elabora mudanças",
                                "Aspereza; Atitudes críticas ou condescendentes.",
                                "Fica entediado com rotina de trabalho; Fica amoado quando restringido; Age de forma independente.",
                                "Falta de influência; Falhar em atingor seus padrões.",
                                "Calor; Comunicação tática; Cooperação de time efetiva; Reconhecimento de sanções existentes.",
                                "Pessoas com um padrão Criativo exibem forças opostas em seu comportamento. Seu desejo de resultados tangíveis é contrabalançado por um impulso igualmente forte para a perfeição e sua agressividade é temperada pela sensibilidade. Embora pensem e reajam rapidamente, são restringidos pelo desejo de explorar todas as soluções possíveis antes de tomar uma decisão.<br><br> As pessoas Criativas preveem resultados quando se concentram em projetos e trazem mudanças. Como os indivíduos com um padrão criativo têm um impulso para a perfeição e demonstram considerável capacidade de planejamento, as mudanças que fazem provavelmente são sólidas, mas o método pode carecer de atenção aos relacionamentos interpessoais.  <br><br>Pessoas Criativas desejam liberdade para explorar e autoridade para examinar e testar novamente as descobertas.  Eles podem tomar decisões diárias rapidamente, mas podem ser extremamente cautelosos ao tomar decisões maiores: \"Será que devo aceitar essa promoção?\" \"Será que devo mudar para outro local?\". Em sua busca por resultados e perfeição, as pessoas Criativas podem não se preocupar com o equilíbrio social. Como resultado, eles podem ser frios e indiferentes."); //Checked
    }

    else if((d <= 4) && (i > 4) && (s <= 4) && (c <= 4)){
        var perfil = new Profile("Influência (I)",
                                "Promotor",
                                "Disposto a aceitar outros.",
                                "Aprovação, popularidade.",
                                "Habilidade de comunicação verbal",
                                "Elogios, oportunidades e favores.",
                                "Alivia tensão; Promove projetos e pessoas, inclusive a si mesmo.",
                                "Elogios, otimismo.",
                                "Se torna descuidado, sentimental; É desorganizado.",
                                "Perda de aceitação social e valor próprio.",
                                "Controle do tempo; Objetividade; Senso de urgência; Controle emocional; Conclusão de promessas e tarefas.",
                                "Os Promotores possuem uma extensa rede de contatos. Geralmente são gregários e socialmente competentes, e desenvolvem amizades com facilidade. Eles raramente antagonizam os outros intencionalmente. Os Promotores buscam ambientes sociais favoráveis ​​onde possam desenvolver e manter seus contatos. Verbalmente habilidosos, eles promovem suas próprias idéias e criam entusiasmo pelos projetos de outros. Com a sua vasta gama de contatos, os Promotores têm acesso às pessoas que os podem ajudar. <br><br>Como os Promotores preferem participar e interagir com outras pessoas nas atividades, eles podem estar menos interessados ​​na realização da tarefa. Eles podem continuar a procurar qualquer situação que envolva conhecer pessoas e socializar, mesmo que seu trabalho exija atenção para atividades mais solitárias. Eles prosperam em reuniões, comitês e conferências. <br><br>Muito otimistas, os Promotores tendem a superestimar a capacidade dos outros. Freqüentemente, chegam a conclusões favoráveis ​​sem considerar todos os fatos. Os Promotores aprenderão a ser objetivos e a enfatizar os resultados com treinamento e direção. A gestão do tempo pode apresentar desafios para os Promotores. Ao estabelecer um limite de tempo para conversa e discussão, eles podem se lembrar da urgência de encerrar e realizar a tarefa"); //Checked
    }

    else if((d <= 5) && (i > 5) && (s <= d) && c <= 4){
        var perfil = new Profile("Influência (I/D)",
                                "Persuasivo",
                                "Confiança nos outros; É entusiasmado.",
                                "Autoridade e prestígio; Símbolos de status.",
                                "Habilidade de verbalizar; Flexibilidade.",
                                "Ser amigável, maneira aberta, adepto da verbalidade.",
                                "Vende e fecha; Delega responsabilidades; É preparado e confiante.",
                                "Entusiasmo; Habilidade de venda; Otimismo.",
                                "Se torna indeciso e facilmente persuadível; Se torna organizado para manter as aparências.",
                                "Ambientes fixos; Relacionamentos complexos.",
                                "Atribuições mais desafiadoras; Atenção a serviços movidos por tarefas e detalhes chave; Análise de dados objetiva.",
                                "Os Persuasivos trabalham com as pessoas, esforçando-se para ser amigáveis ​​enquanto avançam em seus próprios objetivos. Extrovertidos e interessados ​​nas pessoas, os Persuasivos têm a capacidade de ganhar o respeito e a confiança de vários tipos de pessoas. Os Persuasivos podem imprimir seus pensamentos nos outros, atraindo as pessoas a eles e mantendo-os como clientes ou amigos. Essa habilidade é particularmente útil quando os eles vendem a si mesmos ou suas idéias para ganhar posições de autoridade. <br><br>O ambiente mais favorável para os Persuasivos inclui trabalhar com pessoas, receber atribuições desafiadoras e experimentar uma variedade de atividades de trabalho que exigem mobilidade. Eles procuram designações de trabalho que lhes dêem a oportunidade de ter uma boa aparência. Como resultado de sua perspectiva natural positiva, os Persuasivos podem ser muito otimistas sobre os resultados de um projeto e o potencial de outros. Eles também tendem a superestimar sua capacidade de mudar o comportamento dos outros. <br><br>Enquanto os Persuasivos desejam se livrar da rotina e da arregimentação, eles precisam receber dados analíticos de forma sistemática. Depois de alertados para a importância das pequenas coisas, os Persuasivos podem usar as informações para equilibrar seu entusiasmo com uma avaliação realista da situação."); //Checked
    }

    else if((d <= 5) && (i >= 5) && (s <= i) && (c <=4)){
        var perfil = new Profile("Influência (I/S)",
                                "Conselheiro",
                                "Ser aproximável; Mostrando afeição e entendimento",
                                "Amizade; Felicidade.",
                                "Aceitação positiva de outros; Procura o bem nas pessoas.",
                                "Relações interpessoais; Política de portas abertas.",
                                "Permanecendo estável e previsível; Desenvolvendo uma ampla variedade de amizades; Dando ouvidos aos sentimentos dos outros",
                                "Abordagem indireta; Tolerância.",
                                "Se torna flexível de mais e intimidado; É confiável de mais sem nenhuma diferenciação entre as pessoas.",
                                "Pressionar pessoas; Ser acusado de ter causado algum dano.",
                                "Atenção a metas realistas; Iniciativa para completar as tarefas.",
                                "Os Conselheiros são particularmente eficazes na solução de problemas pessoais.  Eles impressionam os outros com seu calor, empatia e compreensão. Seu otimismo torna mais fácil procurar o que há de bom nos outros. Os Conselheiros preferem lidar com os outros construindo relacionamentos duradouros. Como bons ouvintes, com bons ouvidos para os problemas, os Conselheiros oferecem sugestões gentilmente e evitam impor suas idéias aos outros. <br><br>Os Conselheiros tendem a ser excessivamente tolerantes e pacientes com não produtores. Sob pressão, eles podem ter dificuldade em enfrentar problemas de desempenho. Os Conselheiros podem ser indiretos ao emitir ordens, fazer demandas ou disciplinar outros. Ao adotar a atitude de que as pessoas são importantes, os Conselheiros podem colocar menos ênfase na realização de tarefas. Às vezes, eles precisam de ajuda para definir e cumprir prazos realistas. <br><br> Os Conselheiros, muitas vezes, consideram as críticas como uma afronta pessoal, mas respondem bem à atenção e elogios por tarefas concluídas. Quando em uma posição de responsabilidade, os Conselheiros tendem a estar atentos à qualidade das condições de trabalho e fornecer o reconhecimento adequado para os membros de seu grupo,"); //Checked
    }

    else if((d <= i) && (i >= 5) && (s <= 4) && (c >= 5)){
        var perfil = new Profile("Influência (I/C)",
                                "Avaliador",
                                "Se importa em manter boa aparência.",
                                "Vencer na vida com o talento",
                                "Habilidade de iniciar atividades",
                                "Competição por reconhecimento.",
                                "Conquistar objetivos com o time.",
                                "Autoridade; Ingenuidade.",
                                "Se torna incansável, crítico e impaciente.",
                                "Perda e fracasso; Desaprovação dos outros.",
                                "Acompanhamento individual; Empatia ao desaprovar; Ritmo estável.",
                                "Os Avaliadores fazem as idéias Criativas servirem a propósitos práticos. Eles usam métodos diretos para obter resultados. Os Avaliadores são competitivos, mas as outras pessoas tendem a ver os Avaliadores como assertivos, em vez de agressivos, porque eles têm consideração pelos outros. Em vez de dar ordens ou comandos, os Avaliadores envolvem as pessoas na tarefa por meio da persuasão.  Eles atraem a cooperação das pessoas ao seu redor, explicando a razão de ser das atividades propostas. <br><br>Os Avaliadores ajudam outros a visualizar as etapas necessárias para alcançar os resultados. Os Avaliadores geralmente falam a partir de um plano de ação detalhado que desenvolveram para garantir uma progressão ordenada em direção aos resultados. Em sua ânsia de vencer, os Avaliadores podem se tornar impacientes quando seus padrões não são mantidos ou quando é necessário um acompanhamento extensivo. <br><br>Os Avaliadores são bons pensadores críticos. Eles são verbais em suas críticas, e suas palavras, ocasionalmente, podem ser ácidas. Os Avaliadores têm melhor controle da situação se relaxarem e se controlarem. Um axioma útil para conseguir isso é: \"Não dá para ganhar todas\"."); //Checked
    }

    else if((d <= 4) && (i <= 4) && (s >=  5) && (c <= 4)){
        var perfil = new Profile("Estabilidade (S)",
                                "Especialista",
                                "Calculadamente moderado; Acomodar outros.",
                                "Manutenção do status quo; Ambiente controlado.",
                                "Padrões de amizades; Competência.",
                                "Performace consistente; Acomodação de outros.",
                                "Planejamento de curto prazo; É previsível, consistente; Mantem rítmo estável.",
                                "Modestia; Corre poucos riscos; Resistência passiva a inovação.",
                                "Se torna adaptável àqueles em posição de poder e pensa no grupo.",
                                "Caos, mudança e desorganização.",
                                "Discussão pública de suas ideias; Autoconfiança baseada em feedback.",
                                "Os Especialistas são atenciosos, pacientes e estão sempre prontos para ajudar os outros. Por causa de sua personalidade modesta e de boas maneiras, eles se \"vestem bem\" com os outros. O Especialista busca restaurar a harmonia em casa ou no trabalho. Eles procuram minimizar o conflito e criar um ambiente calmo e seguro. Eles são pessoas amigáveis ​​e compassivas, que ouvem pacientemente com empatia. <br><br>Eles constroem relacionamentos profundos e são amigos e parceiros leais. Os Especialistas preferem procedimentos práticos e comprovados que garantam a estabilidade. Eles gostam de padrões familiares e previsíveis que produzem resultados consistentes e confiáveis. <br><br>Eles preferem trabalhar nos bastidores e oferecer apoio, em vez de liderar. Sua capacidade de planejar e seguir procedimentos resulta em um desempenho notável e consistente. A mudança não é fácil para os especialistas - eles não acreditam na mudança apenas pela mudança. Se as razões para a mudança forem totalmente explicadas e os benefícios forem claros, eles a apoiarão para evitar conflitos.");
    }

    else if((d >= 5) && (i < d) && (s >= 5) && (c <= 4)){
        var perfil = new Profile("Estabilidade (S/D)",
                                "Empreendedor",
                                "É trabalhador e diligente; Mostra frustração.",
                                "Realizações pessoais, as veses às custas do objetivo do grupo.",
                                "Habilidade de atingir resultados concretos.",
                                "Responsabilidade pelo próprio trabalho.",
                                "Independência e conclui tarefas de forma eficaz.",
                                "Foco na tarefa e autossuficiência.",
                                "Mostra impaciência e frustração.",
                                "Comprometer resultados com trabalho inferior.",
                                "Claridade nas prioridades das tarefas; Consideração de abordagens opcionais.",
                                "A motivação dos Empreendedores é amplamente interna e flui de objetivos pessoais profundamente sentidos. Seu compromisso com seus próprios objetivos impede uma aceitação automática dos objetivos do grupo. Os Empreendedores precisam ver como podem combinar seus objetivos pessoais com os objetivos da organização. Ao manter o controle sobre a direção de suas vidas, os Empreendedores desenvolvem um forte senso de responsabilidade. <br><br>Os Empreendedores demonstram um grande interesse em seu trabalho e uma busca intensa e contínua por realizações. Eles têm uma opinião elevada sobre seu trabalho e, sob pressão, podem hesitar em delegar tarefas. Em vez disso, eles próprios assumem o trabalho para garantir que as coisas sejam feitas corretamente. Quando delegam, têm tendência a retomar a tarefa se esta não correr de acordo com as suas expectativas. Sua premissa norteadora é: \"se for bem-sucedido, quero o crédito e, se falhar, assumirei a culpa\". <br><br>Um Empreendedor deve se comunicar mais com os outros para expandir seu pensamento além de \"Eu mesmo tenho que fazer isso\" ou \"Eu quero  todo o crédito\". Eles podem precisar de ajuda para encontrar novas abordagens para alcançar os resultados desejados. Os Empreendedores funcionam com eficiência máxima e esperam reconhecimento igual à sua contribuição - altos salários em organizações lucrativas e posições de liderança em outros grupos."); //Checked
    }

    else if((d <= i) && (i >= 5 && i < s) && (s >= 6) && (c <= 4)){
        var perfil = new Profile("Estabilidade (S/I)",
                                "Agente",    
                                "Aceita afeição; Rejeita agressão.",
                                "Aceitação pelo grupo.",
                                "Comprometimento para tolerar e incluir a todos.",
                                "Empatia e amizade.",
                                "Da suporte, harminiza, empatiza; Foca no serviço.",
                                "Bondade.",
                                "Se torna persuasivo, usando informações chave ou amizades se for necessário.",
                                "Conflito; Caos.",
                                "Enfase na percepção de quem eles são e o que podem fazer; Habilidade de dizer não quando for apropriado.",
                                "Os Agentes estão atentos às relações humanas e aos aspectos das tarefas de sua situação de trabalho. Com empatia e apoio, eles são bons ouvintes e conhecidos por seu ouvido atento. Os Agentes fazem as pessoas se sentirem queridas e necessárias. Como os Agentes atendem às necessidades dos outros, as pessoas não temem ser rejeitadas por eles. Os Agentes oferecem amizade e estão dispostos a prestar serviços pelos outros. <br><br>Eles têm excelente potencial para organizar e concluir tarefas com eficácia. Os Agentes naturalmente promovem harmonia e trabalho em equipe e são particularmente bons em fazer pelos outros o que eles acham difícil fazer por si mesmos. <br><br>Os Agentes temem conflito e dissensão. Sua abordagem de apoio pode permitir que outros tolerem uma situação, ao invés de encorajá-los na resolução ativa de problemas. Além disso, a tendência do Agente de adotar um perfil \"baixo\" em vez de enfrentar confrontos abertos com indivíduos agressivos - pode ser percebida como uma falta de \"dureza\". Embora eles estejam preocupados em se encaixar no grupo. Os agentes têm um certo grau de independência."); //Checked
    }

    else if((d >= 5) && (i <= 4) && (s >= 5) && (c >= 5)){
        var perfil = new Profile("Estabilidade (S/CD)",
                                "Investigador",
                                "É desapaixonado; Demonstra auto controle.",
                                "Poder através de papéis formais e posição de autoridade.",
                                "O uso de informações factuais e confiáveis.",
                                "Tenacidade e determinação pessoal.",
                                "Oferece acompanhamento abrangente; Trabalha com determinação em tarefas individuais ou em um pequeno grupo.",
                                "Franqueza; Suspeita dos outros.",
                                "Tende a internalizar o conflito; Apega-se a ressentimentos",
                                "Envolvimento com as massas; Responsabilidade de vender idéias abstratas.",
                                "Flexibilidade; Aceitação dos outros; Envolvimento pessoal com outros.",
                                "Investigadores objetivos e analíticos são \"âncoras de realidade\". Geralmente pouco demonstrativos, eles buscam com calma e firmeza um caminho independente em direção a um objetivo fixo. Os Investigadores têm sucesso em muitas coisas, não por causa da versatilidade, mas devido à sua determinação de seguir adiante. Eles procuram um objetivo ou objetivo claro a partir do qual eles podem desenvolver um plano ordenado e organizar suas ações. Depois que um projeto é iniciado, os Investigadores lutam tenazmente para alcançar seus objetivos. As vezes, é necessária intervenção para mudar sua direção. Como resultado, eles podem ser vistos como obstinados e opinativos.<br><br> Os Investigadores se saem bem com tarefas técnicas desafiadoras nas quais podem usar dados reais para interpretar as informações e tirar conclusões. Eles respondem à lógica em vez da emoção. Ao vender ou fazer marketing de uma idéia, eles têm mais sucesso com um produto concreto.<br><br> Investigadores não estão especialmente interessados ​​em agradar as pessoas e preferem trabalhar sozinhos. Eles podem ser percebidos como frios, bruscos e sem tato. Porque eles valorizam sua própria capacidade de raciocínio, os investigadores avaliam os outros pela maneira como usam os fatos e a lógica. Para aumentar sua eficácia nas interações pessoais, eles precisam desenvolver um maior entendimento ou outras pessoas, especialmente outras emoções."); //Checked
    }  

    else if((d <= 4) && (i <=4) && (s <= 4) && (c >= 5)){
        var perfil = new Profile("Conformidade (C)",
                                "Pensador Objetivo",    
                                "Rejeita agressões interpessoais.",
                                "Exatidão",
                                "Capacidade de pensar logicamente .",
                                "Uso da lógica, de fatos e de dados.",
                                "Define e esclarece; Obtém, avalia e testa informações.",
                                "Tende a analisar excessivamente.",
                                "Tende a se preocupar.",
                                "Críticas e perda do controle das emoções; Ridicularização e atos irracionais.",
                                "Auto-revelação; Discussão pública das suas percepções e opiniões.",
                                "Os Pensadores Objetivos tendem a ter habilidades de pensamento crítico altamente desenvolvidas. Enfatizam a importância dos fatos ao tirar conclusões e planejar ações, e buscam correção e precisão em tudo o que fazem. Para gerenciar suas atividades de trabalho com eficácia, os Pensadores Objetivos geralmente combinam informações intuitivas com os fatos que reuniram. Quando estão em dúvida sobre um curso de ação, evitam a falha pública ao se preparar meticulosamente. Por exemplo, os Pensadores Objetivos irão dominar uma nova habilidade em particular antes de usá-la em uma atividade de grupo. <br><br>Os Pensadores Objetivos preferem trabalhar com pessoas que, como eles, estão interessadas em manter um ambiente de trabalho pacífico. Considerados tímidos por alguns, podem ser reticentes em expressar seus sentimentos. Eles se sentem particularmente incomodados com pessoas agressivas. Apesar de serem moderados, os Pensadores Objetivos têm uma forte necessidade de controlar seu ambiente.  Eles tendem a exercer esse controle indiretamente, exigindo que outros sigam regras e padrões. <br><br>Os Pensadores Objetivos estão preocupados com a resposta \"certa\" e podem ter problemas para tomar decisões em situações ambíguas. Com sua tendência à preocupação, eles podem ficar atolados na \"paralisia da análise\". Quando cometem um erro, os Pensadores Objetivos muitas vezes hesitam em reconhecê-lo. Em vez disso, eles se envolvem em uma busca por informações que apóiem ​​sua posição.");  //Checked
    }

    else if((d <= 4) && (i <= 4) && (s >= 5) && (c >= 5)){
        var perfil = new Profile("Conformidade (C/S)",
                                "Perfeccionista",  
                                "Mostra competência; É restringido e cauteloso.",
                                "Estabilidade; Realizações previsíveis.",
                                "Padrões precisos.",
                                "Precisão; Atenção aos detalhes.",
                                "É consciente; Controle de qualidade; Manutenção de padrões.",
                                "Opções de segurança contra falhas; Dependência exagerada de outras pessoas, produtos e processos que funcionavam no passado.",
                                "Se torna tático e diplomático.",
                                "Ambiente hostil; Antagonismo.",
                                "Flexibilidade de função; Independência e interdependência; Crença no auto-valor.",
                                "Os Perfeccionistas são pensadores e trabalhadores sistemáticos e precisos, que seguem procedimentos em sua vida pessoal e profissional. Extremamente conscienciosos, eles são diligentes em trabalhos que requerem atenção aos detalhes e precisão. Porque desejam condições estáveis ​​e atividades previsíveis, os Perfeccionistas se sentem mais confortáveis ​​em um ambiente de trabalho claramente definido. Eles querem detalhes sobre as expectativas de trabalho, requisitos de tempo e procedimentos de avaliação. <br><br>Os Perfeccionistas podem se prender aos detalhes do processo de tomada de decisão.  Eles podem tomar decisões importantes, mas podem ser criticados pelo tempo que levam para coletar e analisar informações. Embora gostem de ouvir a opinião de seus gerentes, os Perfeccionistas assumem riscos quando têm fatos que podem ser interpretados e usados para se tirar conclusões. <br><br>Os Perfeccionistas avaliam a si mesmos e aos outros por padrões precisos para alcançar resultados concretos enquanto aderem aos procedimentos operacionais padrão. Essa atenção cuidadosa aos padrões e qualidade é valiosa para a organização. Os Perfeccionistas podem definir muito seu valor pelo que fazem e não por quem são como pessoas. Como resultado, eles tendem a reagir aos elogios pessoais pensando: \"O que essa pessoa quer?\". Ao aceitar elogios sinceros, os Perfeccionistas podem aumentar sua autoconfiança.");   //Checked
    }

    else if((d <= 4) && (i >= 5) && (s >= 4) && (c >= 5)){
        var perfil = new Profile("Conformidade (C/I/S)",
                                "Praticante", 
                                "Deseja acompanhar os outros em questão de performace técnica e esforço.",
                                "Crescimento pessoal.",
                                "Posição de status e promoções; Autodisciplina.",
                                "Confiança em sua capacidade para dominar uma nova habilidade; Desenvolvimento de procedimentos e ações adequadas.",
                                "É hábil na solução de problemas técnicos e pessoais; Demonstra proficiência e especialização.",
                                "Expectativas irrealistas em outras pessoas; Coloca os objetivos pessoais em primeiro lugar.",
                                "Torna-se contido; Sensível a críticas.",
                                "Não ser reconhecido como um \"especialista\" em sua área; Ser muito previsível.",
                                "Colaboração genuína para benefício em comum; Delegação de tarefas chaves para indivíduos apropriados.",
                                "Os Praticantes valorizam a proficiência em áreas especializadas. Estimulados pelo desejo de ser \"bons em alguma coisa\", eles monitoram cuidadosamente seu próprio desempenho no trabalho. Embora seu objetivo seja ser \"o\" especialista em uma área, os Praticantes freqüentemente dão a impressão de que sabem algo sobre tudo. Essa imagem é particularmente forte quando verbalizam seu conhecimento sobre uma variedade de assuntos. <br><br>À medida que os Praticantes interagem com os outros, eles projetam um estilo relaxado, diplomático e tranquilo. Essa atitude agradável pode mudar rapidamente em sua própria área de trabalho, quando eles se tornam intensamente focados a fim de atender a altos padrões de desempenho. Por valorizarem a autodisciplina, os Praticantes avaliam os outros com base em sua capacidade de se concentrar no desempenho diário. Eles têm grandes expectativas de si mesmos e dos outros e tendem a verbalizar sua decepção. <br><br>Enquanto eles naturalmente se concentram em desenvolver uma abordagem organizada para trabalhar e aumentar suas próprias habilidades, os Praticantes também precisam ajudar outros a desenvolver habilidades. Além disso, eles precisam aumentar sua apreciação por aqueles que contribuem para o esforço de trabalho, mesmo que não usem os métodos preferidos do Profissional."); //Checked
    }

    var rejectedPatterns = ["7777", "7776", "7775", "7767", "7766", "7765", "7757", "7756", "7755",
                            "7677", "7676", "7675", "7667", "7666", "7665", "7657", "7656", "7655",
                            "7577", "7576", "7575", "7567", "7566", "7565", "7557", "7556", "7555",
                            "6777", "6776", "6775", "6767", "6766", "6765", "6757", "6756", "6755",
                            "6677", "6676", "6675", "6667", "6666", "6665", "6657", "6656", "6655",
                            "6577", "6576", "6575", "6567", "6566", "6565", "6557", "6556", "6555",
                            "5777", "5776", "5775", "5767", "5766", "5765", "5757", "5756", "5755",
                            "5677", "5676", "5675", "5667", "5666", "5665", "5657", "5656", "5655",
                            "5577", "5576", "5575", "5567", "5566", "5565", "5557", "5556", "5555", 
                            "5554", "5544", "5444", "4555", "4544", "4555", "4544", "4455", "4454",
                            "4445", "4444", "4443", "4442", "4441", "4434", "4433", "4432", "4431",
                            "4424", "4423", "4422", "4421", "4414", "4413", "4412", "4411", "4344",
                            "4343", "4342", "4341", "4334", "4333", "4332", "4331", "4324", "4323",
                            "4322", "4321", "4314", "4313", "4312", "4311", "4244", "4243", "4242", 
                            "4241", "4234", "4233", "4232", "4231", "4224", "4223", "4222", "4221",
                            "4214", "4213", "4212", "4211", "4144", "4143", "4142", "4141", "4134", 
                            "4133", "4132", "4131", "4134", "4123", "4122", "4121", "4114", "4113",
                            "4112", "4111", "3444", "3443", "3442", "3441", "3434", "3433", "3432", 
                            "3431", "3424", "3423", "3422", "3421", "3414", "3413", "3412", "3411",
                            "3344", "3343", "3342", "3341", "3334", "3333", "3332", "3331", "3324",
                            "3323", "3322", "3321", "3314", "3313", "3312", "3311", "3244", "3243", 
                            "3242", "3241", "3234", "3233", "3232", "3231", "3224", "3223", "3222", 
                            "3221", "3214", "3213", "3212", "3211", "3144", "3143", "3142", "3141",
                            "3134", "3133", "3132", "3131", "3124", "3123", "3122", "3121", "3114", 
                            "3113", "3112", "3111", "2444", "2443", "2442", "2441", "2434", "2433", 
                            "2432", "2431", "2424", "2423", "2422", "2421", "2414", "2413", "2412", 
                            "2411", "2344", "2343", "2342", "2341", "2334", "2333", "2332", "2331",
                            "2324", "2323", "2322", "2321", "2314", "2313", "2312", "2311", "2244", 
                            "2243", "2242", "2241", "2234", "2233", "2232", "2231", "2224", "2223",
                            "2222", "2221", "2214", "2213", "2212", "2211", "2144", "2143", "2142", 
                            "2141", "2134", "2133", "2132", "2131", "2124", "2123", "2122", "2121", 
                            "2114", "2113", "2112", "2111", "1444", "1443", "1442", "1441", "1434",
                            "1433", "1432", "1431", "1424", "1423", "1422", "1421", "1414", "1413", 
                            "1412", "1411", "1344", "1343", "1342", "1341", "1334", "1333", "1332",
                            "1331", "1324", "1323", "1322", "1321", "1314", "1313", "1312", "1311",
                            "1244", "1243", "1242", "1241", "1234", "1233", "1232", "1231", "1224",
                            "1223", "1222", "1221", "1214", "1213", "1212", "1211", "1144", "1143",
                            "1142", "1141", "1134", "1133", "1132", "1131", "1124", "1123", "1122",
                            "1121", "1114", "1113", "1112", "1111"];
    
    var segmentString = d.toString() + i.toString() + s.toString() + c.toString();
    console.log("String: " + segmentString);

    for(var i = 0; i < rejectedPatterns.length; i++){
        if(segmentString == rejectedPatterns[i]){
            var perfil = new Profile("Nulo - Teste Inválido", 
                                "----------", "----------", "----------",
                                "----------", "----------", "----------",
                                "----------", "----------", "----------",
                                "----------");
        }
    }
    return perfil;
}